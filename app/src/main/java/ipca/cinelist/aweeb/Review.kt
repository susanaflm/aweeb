package ipca.cinelist.aweeb

class Review {

    var text : String? = null
    var score : String? = null
    var animeURL: String? = null
    var userID : String? = null

    constructor(){

    }

    constructor(text: String?, score: String?, animeURL: String?, userID : String?) {
        this.text = text
        this.score = score
        this.animeURL = animeURL
        this.userID = userID
    }
}
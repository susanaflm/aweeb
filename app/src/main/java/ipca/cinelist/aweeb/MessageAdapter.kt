package ipca.cinelist.aweeb

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView
import ipca.cinelist.aweeb.MessageAdapter.ViewHolder

class MessageAdapter : RecyclerView.Adapter<ViewHolder>{


    companion object{
        final const val MSG_TYPE_LEFT : Int = 0
        final const val MSG_TYPE_RIGHT : Int = 1
    }

    var contex : Context
    var chatList : MutableList<Chat> = ArrayList()
    var imageURL : String?= null
    val user = Firebase.auth.currentUser

    constructor(contex: Context, chatList: MutableList<Chat>, imageURL : String) : super() {
        this.contex = contex
        this.chatList = chatList
        this.imageURL = imageURL
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(viewType == MSG_TYPE_RIGHT){
            val view : View = LayoutInflater.from(contex).inflate(R.layout.chat_layout_right, parent,false)
            return  MessageAdapter.ViewHolder(view)
        }else{
            val view : View = LayoutInflater.from(contex).inflate(R.layout.chat_layout_left, parent,false)
            return MessageAdapter.ViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chat : Chat = chatList.get(position)

        holder.show_message.setText(chat.message)
        if(imageURL.equals("default")){
            holder.profile_image.setImageResource(R.mipmap.hm_round)
        }else{
            Glide.with(contex).load(imageURL).into(holder.profile_image)
        }
    }

    override fun getItemCount(): Int {
        return chatList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val show_message = itemView.findViewById<TextView>(R.id.show_message)
        val profile_image = itemView.findViewById<CircleImageView>(R.id.profile_user1)
    }

    override fun getItemViewType(position: Int): Int {
        val referenceChats : DatabaseReference


        if(chatList.get(position).sender.equals(user?.uid)){
            return MSG_TYPE_RIGHT
        }else{
            return MSG_TYPE_LEFT
        }
    }
}
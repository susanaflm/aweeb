package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.*
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class ReviewDetailsActivity : AppCompatActivity() {

    companion object{
        const val IMAGEURL = "url"
        const val ANIMEID = "id"
    }


    private lateinit var auth: FirebaseAuth
    private lateinit var referenceReviews : DatabaseReference
    private lateinit var reference : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_details)

        auth = FirebaseAuth.getInstance()

        val imageURL = intent.getStringExtra(IMAGEURL)

        var score = 5

        val rateTextView = findViewById<TextView>(R.id.RateText)
        val reviewView = findViewById<EditText>(R.id.reviewText)
        val rateUpButton = findViewById<Button>(R.id.rateUp)
        val rateDownButton = findViewById<Button>(R.id.rateDown)
        val confirmButton = findViewById<Button>(R.id.confirmButton)
        val animeImageView = findViewById<ImageView>(R.id.animeImageReview)

        Glide.with(this).load(imageURL).into(animeImageView)

        rateTextView.text = "Rate: ${score.toString()}"

        rateUpButton.setOnClickListener{
            if (score < 10)
                score += 1
            rateTextView.text = "Rate: ${score.toString()}"
        }

        rateDownButton.setOnClickListener {
            if (score > 0)
                score -= 1
            rateTextView.text = "Rate: ${score.toString()}"
        }

        confirmButton.setOnClickListener {
            if (TextUtils.isEmpty(reviewView.text.toString()))
                Toast.makeText(this, "You need to add something to your review", Toast.LENGTH_SHORT).show()
            else
                addReview(reviewView.text.toString(), score, imageURL.toString())
        }
    }

    private fun addReview(text: String, score: Int, animeURL: String)
    {
        referenceReviews = FirebaseDatabase.getInstance().getReference("Reviews").push()

        val hashmap = HashMap<String, String>()

        hashmap.put("text", text)
        hashmap.put("score", score.toString())
        hashmap.put("animeURL", animeURL)
        hashmap.put("userID", auth.currentUser!!.uid)

        referenceReviews.setValue(hashmap)
                .addOnCompleteListener(this@ReviewDetailsActivity) { task ->
                    if (task.isSuccessful){
                        Toast.makeText(this, "Review successfully submitted!", Toast.LENGTH_SHORT).show()
                        val intent = Intent(this, PostActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    else
                        Toast.makeText(this, "There has been an error submitting your review!", Toast.LENGTH_SHORT).show()
                }
    }
}
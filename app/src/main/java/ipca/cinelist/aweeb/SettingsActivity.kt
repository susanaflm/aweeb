package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView
import kotlin.reflect.jvm.internal.ReflectProperties

class SettingsActivity : AppCompatActivity() {

    val keys : MutableList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        /*BUTTONS*/
        val messageButton = findViewById<ImageView>(R.id.message_button3)
        val searchButton = findViewById<ImageView>(R.id.search_button3)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button3)
        val userAvi3 = findViewById<CircleImageView>(R.id.userIc3)
        val deleteAccount = findViewById<TextView>(R.id.deleteButton)
        //settings buttons
        val changePasswordButton = findViewById<RelativeLayout>(R.id.button_changePassword)
        val changeEmailButton = findViewById<RelativeLayout>(R.id.button_changeEmail)
        val aboutUsButton = findViewById<RelativeLayout>(R.id.button_aboutAweeb)

        /*Variables*/
        val username = findViewById<TextView>(R.id.userIdRef)
        val user = Firebase.auth.currentUser!!
        val reference = Firebase.database.getReference("Users").child(user.uid)
        val referenceReviews = Firebase.database.getReference("Reviews")

        //gets from the data base the image and username of the current user
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val currentUser = snapshot.getValue(User ::class.java)

                //so it displays the users's info on his profile
                username.setText(currentUser?.username)

                if(currentUser?.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    userAvi3.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@SettingsActivity).load(currentUser?.imageURL).into(userAvi3)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        deleteAccount.setOnClickListener {
            user.delete()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        Toast.makeText(this, "User account deleted.", Toast.LENGTH_SHORT).show()

                        reference.removeValue()

                        val referenceReviews = FirebaseDatabase.getInstance().getReference("Reviews")
                        referenceReviews.addValueEventListener(object : ValueEventListener{
                            override fun onDataChange(snapshot: DataSnapshot) {
                                for (child in snapshot.children){
                                    val review = child.getValue(Review::class.java)

                                    if (review?.userID.equals(user.uid))
                                        keys.add(child.toString())
                                }
                                for (key in keys){
                                    val referenceToDelete = FirebaseDatabase.getInstance().getReference("Reviews").child(key)
                                    referenceToDelete.removeValue()
                                        .addOnCompleteListener { task ->
                                            if (task.isSuccessful){
                                                val intent = Intent(this@SettingsActivity, RegisterActivity::class.java)
                                                startActivity(intent)
                                                finish()
                                            }
                                        }
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {

                            }

                        })
                    }
                }
        }

        //ALL BUTTON ACTIONS
        //settings buttons
        aboutUsButton.setOnClickListener{
            val intent = Intent(this, SettingsAboutUsActivity :: class.java)
            startActivity(intent)
        }

        changeEmailButton.setOnClickListener{
            val intent = Intent(this, SettingsChangeUsernameActivity :: class.java)
            startActivity(intent)
        }

        changePasswordButton.setOnClickListener{
            val intent = Intent(this, SettingsChangePasswordActivity :: class.java)
            startActivity(intent)
        }

        /*ALL UI BUttons*/
        userAvi3.setOnClickListener{
            val intent = Intent(this, UserProfileActivity :: class.java)
            startActivity(intent)
        }

        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity :: class.java)
            startActivity(intent)
        }

        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity :: class.java)
            startActivity(intent)
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }
}
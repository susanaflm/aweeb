package ipca.cinelist.aweeb

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import okhttp3.OkHttpClient
import okhttp3.Request
import java.lang.Exception
import java.net.URL
import kotlin.coroutines.resume


class Backend {
    companion object {
        suspend fun getTopAiringAnime(page : String) : String = suspendCoroutine {
            var result = ""
            try {
                val client = OkHttpClient()

                val request = Request.Builder()
                    .url("https://jikan1.p.rapidapi.com/top/anime/${page}/airing")
                    .get()
                    .addHeader("x-rapidapi-key", "a7035a7b84msh1d0914a85688853p1f4caejsn3d2e1e87308f")
                    .addHeader("x-rapidapi-host", "jikan1.p.rapidapi.com")
                    .build()

                val response = client.newCall(request).execute()

                result = response.body()?.string()?:""

            }catch (e:Exception){
                e.printStackTrace()
                result = "No internet!"
            }
            it.resume(result)
        }

        suspend fun getSearchAnime(search : String) : String = suspendCoroutine {
            var result = ""
            var searchLink = search.replace(" ", "%20")
            try {
                val client = OkHttpClient()
                val request = Request.Builder()
                        .url("https://jikan1.p.rapidapi.com/search/anime?q=${search}&type=tv")
                        .get()
                        .addHeader("x-rapidapi-key", "f805a2c8d4msh639b3770ecf51e5p15d4a7jsnb6590d9ab1a1")
                        .addHeader("x-rapidapi-host", "jikan1.p.rapidapi.com")
                        .build()

                val response = client.newCall(request).execute()
                result = response.body()?.string()?:""

            }catch (e:Exception){
                e.printStackTrace()
                result = "No internet!"
            }
            it.resume(result)
        }

        fun getBitmapFromUrl(urlString: String, onBitmapResult : (Bitmap) -> Unit){
            object : AsyncTask<Unit, Unit, Bitmap>(){
                override fun doInBackground(vararg params: Unit?): Bitmap {
                    val input = URL(urlString).openStream()
                    var bmp = BitmapFactory.decodeStream(input)
                    return bmp
                }

                override fun onPostExecute(result: Bitmap?) {
                    super.onPostExecute(result)
                    result?.let{
                        onBitmapResult.invoke(result)
                    }
                }
            }.execute(null,null,null)
        }
    }
}
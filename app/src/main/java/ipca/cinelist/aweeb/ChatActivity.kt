package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*
import kotlin.collections.HashMap

class ChatActivity : AppCompatActivity() {
    private lateinit var referenceChats : DatabaseReference

    val chats : MutableList<Chat> = ArrayList()
    val user = Firebase.auth.currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        //buttons
        val returnButton = findViewById<ImageView>(R.id.button_return)
        val profile_image = findViewById<CircleImageView>(R.id.profile_image2)
        val username = findViewById<TextView>(R.id.friend_username2)
        val sendButton = findViewById<ImageView>(R.id.button_send)
        val recyclerView = findViewById<RecyclerView>(R.id.displayMessages)
        val messageBox = findViewById<EditText>(R.id.messageBox)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val intent : Intent = getIntent()
        var userId : String = intent.getStringExtra("userid").toString()

        val reference = Firebase.database.getReference("Users").child(userId.toString())
        recyclerView.setHasFixedSize(true)

        val linearLayoutManager = LinearLayoutManager(applicationContext)
        linearLayoutManager.stackFromEnd = true
        recyclerView.layoutManager = linearLayoutManager

        sendButton.setOnClickListener{
            val message : String? = messageBox.text.toString()

            if(!message.equals("")){
                if (userId != null && message != null) {
                    sendMessage(user!!.uid, userId, message)
                }
            }else{
                Toast.makeText(this@ChatActivity, "Wrong",Toast.LENGTH_SHORT).show()
            }
            messageBox.setText("")
        }


        reference.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                 val currentUser = snapshot.getValue(User::class.java)
                 username.setText(currentUser?.username)

                if(currentUser?.imageURL.equals("default")){
                    profile_image.setImageResource(R.mipmap.hm_round)
                }else{
                    Glide.with(this@ChatActivity).load(currentUser?.imageURL).into(profile_image)
                }

                readMessages(user!!.uid, userId, currentUser?.imageURL.toString(), recyclerView)

            }
            override fun onCancelled(error: DatabaseError) {
            }
        })

        //return to main messages
        returnButton.setOnClickListener{
            finish()
        }

    }
    fun sendMessage(sender : String, receiver : String, message : String){
        var userId : String ?= intent.getStringExtra("userid")
        referenceChats = FirebaseDatabase.getInstance().getReference("Chats").push()
        val hashMap = HashMap<String, String>()

        hashMap.put("sender", sender)
        hashMap.put("receiver", receiver)
        hashMap.put("message", message)

        referenceChats.setValue(hashMap)
    }

    fun readMessages(id : String, otherId : String, imageURL : String, recyclerView : RecyclerView){
        referenceChats = FirebaseDatabase.getInstance().getReference("Chats")
        referenceChats.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                chats.clear()

                for (dataSnapshot : DataSnapshot in snapshot.children) {
                    val chat = dataSnapshot.getValue(Chat::class.java)
                    if(chat?.receiver.equals(id) && chat?.sender.equals(otherId) ||
                            chat?.receiver.equals(otherId) && chat?.sender.equals(id)) {
                        if (chat != null) {
                            chats.add(chat)
                        }
                    }
                }
                val messageAdapter = MessageAdapter(this@ChatActivity, chats, imageURL)
                recyclerView.adapter = messageAdapter
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })
    }
}
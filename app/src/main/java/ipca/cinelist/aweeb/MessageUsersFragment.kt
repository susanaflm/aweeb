package ipca.cinelist.aweeb

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


class MessageUsersFragment : Fragment() {

    var userList : MutableList<User> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_message_users, container, false)

        val rec : RecyclerView = view.findViewById(R.id.friendListDisplay)
        rec.layoutManager = LinearLayoutManager(context)

        readUsers()
        return view
    }

    private fun readUsers(){
        val user = Firebase.auth.currentUser
        val reference = Firebase.database.getReference("Users")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()

                for (dataSnapshot : DataSnapshot in snapshot.children) {
                    val currentUser = dataSnapshot.getValue(User:: class.java)
                    val thisUser = user?.displayName.toString()

                    if(currentUser != null && currentUser.username != thisUser) {
                        userList.add(currentUser)
                    }
                }
                if(context != null) {
                    val userAdapter = UserAdapter(context!!, userList)
                    val rec : RecyclerView = view!!.findViewById(R.id.friendListDisplay)
                    rec.adapter = userAdapter
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }
}
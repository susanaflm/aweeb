package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class RegisterActivity : AppCompatActivity() {

    private val TAG = "RegisterActivity"
    private lateinit var auth: FirebaseAuth
    private lateinit var reference : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val email = findViewById<EditText>(R.id.emailReg)
        val password = findViewById<EditText>(R.id.passwordReg)
        val registerButton = findViewById<Button>(R.id.registerButton)
        val textViewSignIn = findViewById<TextView>(R.id.signInText)

        auth = FirebaseAuth.getInstance()

        registerButton.setOnClickListener{
            val emailText = email.text.toString()
            val passwordText = password.text.toString()
            if (TextUtils.isEmpty(emailText) || TextUtils.isEmpty(passwordText))
                Toast.makeText(this, "Both fields are required!", Toast.LENGTH_SHORT).show()
            else if (passwordText.length < 6)
                Toast.makeText(this, "Password must be at least 6 characters long!", Toast.LENGTH_SHORT).show()
            else
                register(emailText, passwordText)
        }

        textViewSignIn.setOnClickListener{
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }
    }

    private fun register(email : String, password : String){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "createUserWithEmail:success")
                        val user = auth.currentUser
                        val userID = user!!.uid;

                        reference = FirebaseDatabase.getInstance().getReference("Users").child(userID)

                        var hashmap = HashMap<String, String>()
                        hashmap.put("id",userID)
                        hashmap.put("username", "")
                        hashmap.put("imageURL", "default")
                        hashmap.put("favoriteAnimes", "")
                        hashmap.put("seenAnimes", "")
                        hashmap.put("bio", "")

                        reference.setValue(hashmap).addOnCompleteListener(this){ done ->
                            if (done.isSuccessful) {
                                val intent = Intent(this, UserRegisterActivity::class.java)
                                startActivity(intent)
                            }
                        }

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()

                    }
                }
    }
}
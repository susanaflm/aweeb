package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text

class SearchActivity : AppCompatActivity() {

    var searchResults : MutableList<Anime> = ArrayList()
    var searchAdapter = SearchAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        /*BUTTONS*/
        val messageButton = findViewById<ImageView>(R.id.message_button2)
        val settingsButton = findViewById<ImageView>(R.id.settings_button2)
        val searchButton = findViewById<ImageView>(R.id.search_button2)
        val timelineButton = findViewById<ImageView>(R.id.timeline_button2)
        val searchBar = findViewById<EditText>(R.id.editTextSearch)
        val buttonSearch = findViewById<ImageView>(R.id.buttonSearch)
        val aviButton = findViewById<CircleImageView>(R.id.userIc2)

        val listViewSearchResult = findViewById<ListView>(R.id.listViewSearch)
        listViewSearchResult.adapter = searchAdapter

        val user = Firebase.auth.currentUser
        val reference = Firebase.database.getReference("Users").child(user?.uid.toString())

        //gets from the data base the image  of the current user
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var currentUser = snapshot.getValue(User ::class.java)

                if(currentUser?.imageURL != null &&  currentUser.imageURL.equals("default")) {
                    aviButton.setImageResource(R.mipmap.hm_round)
                }
                else{
                    Glide.with(this@SearchActivity).load(currentUser?.imageURL).into(aviButton)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        buttonSearch.isClickable = true
        buttonSearch.setOnClickListener {
            searchResults.clear()

            GlobalScope.async {
                val search = searchBar.text.toString()
                if (!TextUtils.isEmpty(search)){
                    Log.d("Search", searchBar.text.toString())

                    val result = Backend.getSearchAnime(search)
                    if (result == "No internet!"){
                        runOnUiThread{
                            Toast.makeText(this@SearchActivity, "No Internet!", Toast.LENGTH_LONG).show()
                        }
                    }else {
                        val jsonObject = JSONObject(result)
                        val jsonArray : JSONArray = jsonObject.getJSONArray("results")
                        for (i in 0..jsonArray.length() - 1){
                            val jsonAnime = jsonArray.getJSONObject(i)
                            val anime = Anime.fromJson(jsonAnime)
                            anime.airing = jsonAnime.getString("airing")

                            searchResults.add(anime);
                        }
                        runOnUiThread{
                            searchAdapter.notifyDataSetChanged()
                        }
                    }
                }
                else
                    Toast.makeText(this@SearchActivity, "Search Parameters are needed!", Toast.LENGTH_SHORT).show()
            }
        }



        //ALL BUTTON ACTIONS
        aviButton.setOnClickListener{
            val intent = Intent(this, UserProfileActivity :: class.java)
            startActivity(intent)
            finish()
        }

        //message activity
        messageButton.setOnClickListener{
            val intent = Intent(this, MessageActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //settings activity
        settingsButton.setOnClickListener{
            val intent = Intent(this, SettingsActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //posts activity
        timelineButton.setOnClickListener{
            val intent = Intent(this, PostActivity :: class.java)
            startActivity(intent)
            finish()
        }
        //search activity
        searchButton.setOnClickListener{
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    inner class SearchAdapter : BaseAdapter() {
        override fun getCount(): Int {
            return searchResults.count()
        }

        override fun getItem(position: Int): Any {
            return searchResults[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = layoutInflater.inflate(R.layout.row_anime, parent, false)

            val imageViewAnime = rowView.findViewById<ImageView>(R.id.imageViewSearchAnime)
            val textViewAnimeName = rowView.findViewById<TextView>(R.id.textViewAnimeName)
            val textViewAnimeAiring = rowView.findViewById<TextView>(R.id.textViewAnimeAiring)

            textViewAnimeName.text = searchResults[position].title
            val text = textViewAnimeAiring.text.toString()

            textViewAnimeAiring.text = text + searchResults[position].airing

            if ((searchResults[position].mainPicture?:"").contains("http")){
                Backend.getBitmapFromUrl(searchResults[position].mainPicture!!){
                    imageViewAnime.setImageBitmap(it)
                }
            }

            rowView.isClickable = true
            rowView.setOnClickListener{
                val intent = Intent(this@SearchActivity, AnimeDetailsActivity::class.java)

                intent.putExtra(AnimeDetailsActivity.TITLE, searchResults[position].title)
                intent.putExtra(AnimeDetailsActivity.NUMEPISODES, searchResults[position].episodesCount)
                intent.putExtra(AnimeDetailsActivity.AVERAGERATING, searchResults[position].averageRating)
                intent.putExtra(AnimeDetailsActivity.USERRATING, searchResults[position].userRating)
                intent.putExtra(AnimeDetailsActivity.IMAGEURL, searchResults[position].mainPicture)
                intent.putExtra(AnimeDetailsActivity.ID, searchResults[position].id)

                startActivity(intent)
            }
            return rowView
        }
    }
}
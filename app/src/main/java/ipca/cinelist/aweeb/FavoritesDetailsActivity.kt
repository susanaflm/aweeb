package ipca.cinelist.aweeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class FavoritesDetailsActivity : AppCompatActivity() {

    private lateinit var reference : DatabaseReference

    var animes : MutableList<Anime> = ArrayList()
    var animeAdapter = AnimeAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animelist)
        val gridView = findViewById<GridView>(R.id.gridViewAnime)
        gridView.adapter = animeAdapter

        val user = Firebase.auth.currentUser

        reference = FirebaseDatabase.getInstance().getReference("Users").child(user!!.uid).child("favoriteAnimes")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                animes.clear()

                for (child in snapshot.children) {
                    val anime = child.getValue(Anime::class.java)

                    anime?.let { animes.add(it) }
                }
                runOnUiThread{
                    animeAdapter.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

    }

    inner class AnimeAdapter : BaseAdapter() {
        override fun getCount(): Int {
            return animes.size
        }

        override fun getItem(position: Int): Any {
            return animes[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val gridView = layoutInflater.inflate(R.layout.grid_anime, parent, false)

            val imageViewAnime = gridView.findViewById<ImageView>(R.id.imageAnime)

            if ((animes[position].mainPicture ?: "").contains("http")) {
                Backend.getBitmapFromUrl(animes[position].mainPicture!!) {
                    imageViewAnime.setImageBitmap(it)
                }
            }

            gridView.isClickable = true
            gridView.setOnClickListener {
                val intent = Intent(this@FavoritesDetailsActivity, AnimeDetailsActivity::class.java)

                intent.putExtra(AnimeDetailsActivity.TITLE, animes[position].title)
                intent.putExtra(AnimeDetailsActivity.NUMEPISODES, animes[position].episodesCount)
                intent.putExtra(AnimeDetailsActivity.AVERAGERATING, animes[position].averageRating)
                intent.putExtra(AnimeDetailsActivity.USERRATING, animes[position].userRating)
                intent.putExtra(AnimeDetailsActivity.IMAGEURL, animes[position].mainPicture)
                intent.putExtra(AnimeDetailsActivity.ID, animes[position].id)

                startActivity(intent)
            }

            return gridView
        }
    }
}
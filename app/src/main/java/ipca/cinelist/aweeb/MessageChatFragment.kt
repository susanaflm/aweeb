package ipca.cinelist.aweeb

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase


class MessageChatFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_message_chat, container, false)
        val rec : RecyclerView = view.findViewById(R.id.messages_view)
        rec.setHasFixedSize(true)

        rec.layoutManager = LinearLayoutManager(context)
        val user = Firebase.auth.currentUser
        val userList : MutableList<String> = ArrayList()
        val reference : DatabaseReference = FirebaseDatabase.getInstance().getReference("Chats")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()

                for (dataSnapshot : DataSnapshot in snapshot.children) {
                    val chat = dataSnapshot.getValue(Chat::class.java)
                    if(chat?.sender.equals(user?.uid)){
                        userList.add(chat?.receiver!!)
                    }
                    if(chat?.receiver.equals(user?.uid)){
                        userList.add(chat?.sender!!)
                    }
                }
                readChats(rec)
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })

        return view
    }

    fun readChats(rec : RecyclerView){
        val users : MutableList<User> = ArrayList()
        val reference = FirebaseDatabase.getInstance().getReference("Users")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                users.clear()

                for (dataSnapshot : DataSnapshot in snapshot.children) {
                    val user = dataSnapshot.getValue(User::class.java)

                    for(id in users){
                        if(user!!.id!!.equals(id)){
                            if(users.size != 0){
                                for(dUser in users){
                                    if(!user.id.equals(dUser.id)){
                                        users.add(user)
                                    }
                                }
                            }else{
                                users.add(user)
                            }
                        }
                    }
                }
                val userAdapter = UserAdapter(context!!, users)
                rec.adapter = userAdapter
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })
    }
}